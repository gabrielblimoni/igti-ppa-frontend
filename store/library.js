export const state = () => ({
    libraries: null,
    selectedLibrary: null,
    createLibrary: null,
    loading: false,
})

export const mutations = {
    setLibraries: (state, libraries) => state.libraries = libraries,
    setSelectedLibrary: (state, library) => state.selectedLibrary = library,
    unsetSelectedLibrary: (state) => state.selectedLibrary = null,

    setCreateLibrary: (state) => state.createLibrary = true,
    unsetCreateLibrary: (state) => state.createLibrary = false,

    setLibraryBooks: (state, { books, libraryId }) => {
        const library = state.libraries.filter(l => l._id == libraryId)[0]
        library.books = books
        library.booksLoaded = true
    },

    updateLibrary: (state, library) => {
        state.libraries.forEach(l => {
            if (l._id == library._id) {
                const index = state.libraries.indexOf(l)
                library.books = l.books
                library.booksLoaded = l.booksLoaded
                state.libraries.splice(index, 1, library)
                return
            }
        })
    },
    updateBook: (state, book) => {
        state.libraries.forEach(l => {
            if (l._id == book.libraryId) {
                l.books.forEach(b => {
                    if (b._id == book._id) {
                        const bIndex = l.books.indexOf(b)
                        l.books.splice(bIndex, 1, book)
                    }
                })
                return
            }
        })
    },
    removeBook: (state, book) => {
        state.libraries.forEach(l => {
            if (l._id == book.libraryId) {
                l.books.forEach(b => {
                    if (b._id == book._id) {
                        const bIndex = l.books.indexOf(b)
                        l.books.splice(bIndex, 1)
                    }
                })
                return
            }
        })
    },
    addBook: (state, book) => {
        state.libraries.forEach(l => {
            if (l._id == book.libraryId) {
                l.books.push(book)
                return
            }
        })
    },
    removeLibrary: (state, library) => {
        state.libraries.forEach(l => {
            if (l._id == library._id) {
                const index = state.libraries.indexOf(l)
                state.libraries.splice(index, 1)
                return
            }
        })
    },
    addLibrary: (state, library) => state.libraries.push(library),
    setLoading: state => state.loading = true,
    unsetLoading: state => state.loading = false,
}

export const actions = {
    async updateLibrary({ commit }, library) {
        try {
            commit('setLoading')
            const { data } = await this.$axios.patch(`/library/${library._id}`, library)
            commit('updateLibrary', data)
            commit('unsetLoading')
            return data
        } catch (error) {
            return null
        }
    },
    
    async createLibrary({ commit }, library) {
        try {
            commit('setLoading')
            const { data } = await this.$axios.post(`/library`, library)
            commit('addLibrary', data)
            commit('unsetLoading')
            return data
        } catch (error) {
            return null
        }
    },

    async removeLibrary({ commit }, library) {
        try {
            commit('setLoading')
            const { data } = await this.$axios.delete(`/library/${library._id}`)
            commit('removeLibrary', library)
            commit('unsetLoading')
            return data
        } catch (error) {
            return null
        }
    },

    async getAllLibraries({ commit }) {
        try {
            const { data } = await this.$axios.get('/library')
            commit('setLibraries', data)
            return data
        } catch (error) {
            return null
        }
    },

    async getLibraryBooks({ state, commit }, libraryId) {
        try {
            // without going to API
            const library = state.libraries.filter(l => l._id == libraryId)[0]
            if (library.booksLoaded) {
                return library.books
            }
            
            commit('setLoading')
            // going to API
            const { data } = await this.$axios.get(`/library/${libraryId}`)
            commit('setLibraryBooks', {
                books: data.books,
                libraryId
            })
            commit('unsetLoading')
            return data.books
        } catch (error) {
            return null
        }
    },
}