export const state = () => ({
    selectedBook: null,
    createBook: null,
    loading: false,
})

export const mutations = {
    setSelectedBook: (state, book) => state.selectedBook = book,
    unsetSelectedBook: (state) => state.selectedBook = null,

    setCreateBook: (state) => state.createBook = true,
    unsetCreateBook: (state) => state.createBook = false,

    setLoading: state => state.loading = true,
    unsetLoading: state => state.loading = false,
}

export const actions = {
    async updateBook({ commit }, book) {
        try {
            commit('setLoading')
            const { data } = await this.$axios.patch(`/book/${book._id}`, book)
            this.commit('library/updateBook', data)
            commit('unsetLoading')
            return data
        } catch (error) {
            console.log(error)
            return null
        }
    },
    
    async createBook({ commit }, book) {
        try {
            const libraryId = this.state.library.selectedLibrary._id
            commit('setLoading')
            const { data } = await this.$axios.post(`/book/${libraryId}`, book)
            this.commit('library/addBook', data[0])
            commit('unsetLoading')
            return data[0]
        } catch (error) {
            return null
        }
    },

    async removeBook({ commit }, book) {
        try {
            commit('setLoading')
            const { data } = await this.$axios.delete(`/book/${book._id}`)
            this.commit('library/removeBook', book)
            commit('unsetLoading')
            return data
        } catch (error) {
            return null
        }
    },

    async getAllLibraries({ commit }) {
        try {
            const { data } = await this.$axios.get('/book')
            commit('setLibraries', data)
            return data
        } catch (error) {
            return null
        }
    },

    async getBookBooks({ state, commit }, bookId) {
        try {
            // without going to API
            const book = state.libraries.filter(l => l._id == bookId)[0]
            if (book.booksLoaded) {
                return book.books
            }

            // going to API
            const { data } = await this.$axios.get(`/book/${bookId}`)
            commit('setBookBooks', {
                books: data.books,
                bookId
            })
            return data.books
        } catch (error) {
            return null
        }
    },
}